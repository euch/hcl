
val akkaVersion = "2.5.2"

val project = Project(
    id = "hcl",
    base = file(".")
  )
  .settings(
    name := "hcl",
    version := "2.7",
    scalaVersion := "2.12.2",
    scalacOptions in Compile ++= Seq("-encoding", "UTF-8", "-target:jvm-1.8", "-deprecation", "-feature", "-unchecked", "-Xlog-reflective-calls", "-Xlint"),
    javacOptions in Compile ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint:unchecked", "-Xlint:deprecation"),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-remote" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-metrics" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
      "io.kamon" % "sigar-loader" % "1.6.6-rev002",
      "org.scalikejdbc" %% "scalikejdbc"        % "2.5.+",
      "com.twitter" % "chill-akka_2.12" % "0.9.2",
      "com.zaxxer" % "HikariCP" % "2.6.1",
      "org.postgresql" % "postgresql" % "42.0.0",
      "org.apache.commons" % "commons-lang3" % "3.5",
      "org.scala-lang.modules" % "scala-xml_2.12" % "1.0.6",
      "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.18"
    ),
    javaOptions in run ++= Seq(
      "-Xms128m", "-Xmx1024m", "-Djava.library.path=./target/native"),
    Keys.fork in run := true,
    mainClass in (Compile, run) := Some("org.euch.hcl.App"),
    licenses := Seq(("CC0", url("http://creativecommons.org/publicdomain/zero/1.0")))
  )

fork in run := true
