package org.euch.hcl.services

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import com.typesafe.config.ConfigFactory

import scala.language.postfixOps

//#seed
class Seed extends Actor with ActorLogging {

  def receive = {
    case wtf => log.warning(s"Received unsupported msg: $wtf")
  }

}

//#seed
object Seed {

  def main(args: Array[String]): Unit = {
    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [seed]")).
      withFallback(ConfigFactory.load())

    val system = ActorSystem("ClusterSystem", config)
    system.actorOf(Props[Seed], name = "seed")
  }

}