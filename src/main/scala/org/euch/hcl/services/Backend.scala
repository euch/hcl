package org.euch.hcl.services

import akka.NotUsed
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.euch.hcl.JobStateFSM.changeJobState
import org.euch.hcl._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

//#backend
class Backend extends Actor with ActorLogging {

  import scala.concurrent.ExecutionContext.Implicits.global

  val sinkWithFailed: Sink[Job, Future[Set[Job]]] = Sink.fold[Set[Job], Job](Set.empty[Job])(_ ++ Set(_))
  private val cluster = Cluster(context.system)
  private val jobExec: Flow[Job, Job, NotUsed] = Flow[Job].map(jobClosure(_, parsers.exec))
  private val jobSave: Flow[Job, Job, NotUsed] = Flow[Job].map(jobClosure(_, Model.saveJobResult))
  private val findFailedJobs: Flow[Job, Job, NotUsed] = Flow[Job].filterNot(_.state.isInstanceOf[Result])
  private val updateLockedJobs: Flow[Job, Job, NotUsed] = Flow[Job].map(job => job.state match {
    case RetryOnUnlock(roundsToSkip) if roundsToSkip <= 0 => changeJobState(job, RetryUnlocked)
    case RetryOnUnlock(roundsToSkip) if roundsToSkip > 0 => changeJobState(job, RetryOnUnlock(roundsToSkip - 1))
    case _ => job
  })


  override def preStart(): Unit = {
    log.info("Backend is up")
    cluster.subscribe(self, classOf[MemberUp])
  }

  override def postStop(): Unit = {
    cluster.unsubscribe(self)
    log.info("Backend is down")
  }

  def receive: PartialFunction[Any, Unit] = {
    case JobsForBackend(jobs) =>
      log.info(s"Got initial set of jobs to process:\n${jobs.mkString("\n")}")
      processJobs(jobs, sender)
    case state: CurrentClusterState =>
      registerBackend(context, state.members, BackendIsUp(self))
    case MemberUp(member) =>
      registerBackend(context, member, BackendIsUp(self))
    case wtf =>
      log.warning(s"Received unsupported msg in default context: $wtf")
  }

  private def jobClosure(job: Job, fx: Job => Job): Job = {
    try {
      fx(job)
    } catch {
      case th: Throwable =>
        changeJobState(job, BackendFailure(throwable = th, failedBackendRefs = job.state match {
          case RetryImmediately(fbs) => Set(self) ++ fbs
          case _ => Set.empty
        }))
    }
  }

  private def emergStop(msg: String) {
    log.error(msg)
    Option(context) match {
      case Some(ctx) => ctx stop self
      case None =>
    }
  }

  implicit val materializer = ActorMaterializer()

  import akka.pattern.ask

  private def stream(jobs: Set[Job]) = Source[Job](jobs)
    .via(updateLockedJobs)
    .via(jobExec)
    .via(jobSave)
    .via(findFailedJobs)
    .toMat(sinkWithFailed)(Keep.right)

  private def processJobs(jobs: Set[Job], replyTo: ActorRef): Unit = {
    stream(jobs)
      .run()
      .onComplete {
        case Success(failedJobs) =>
          implicit val timeout = Timeout(5 seconds)
          (replyTo ? JobsStatusFromBackend(failedJobs)).mapTo[JobsDiffForBackend].onComplete {
            case Success(changes) => processJobs(tuneJobsList(jobs, changes), replyTo)
            case _ => emergStop("No reply from balancer, stopping")
          }
        case Failure(f) =>
          f.printStackTrace()
          emergStop("general backend failure")
      }
  }

  private def tuneJobsList(currentJobs: Set[Job], changes: JobsDiffForBackend) = {
    val newJobs = currentJobs.filterNot(j => changes.targetsToWithdraw.contains(j.target)) ++ changes.jobsToAdd
    if (newJobs != newJobs) log.info(s"Got changes in jobs set:\n${newJobs.mkString("\n")}")
    newJobs
  }

}

//#backend

object Backend {


  def main(args: Array[String]): Unit = {
    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [backend]")).
      withFallback(ConfigFactory.load())

    import akka.actor.OneForOneStrategy
    import akka.actor.SupervisorStrategy.Restart

    import scala.concurrent.duration._
    OneForOneStrategy(10, 2.minutes) {
      case _ => Restart
    }
    val backendRef = ActorSystem("ClusterSystem", config).
      actorOf(Props[Backend], name = "backend")

    //    scala.sys.addShutdownHook {
    //      backendRef ! Reset
    //    }
  }
}




