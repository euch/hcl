package org.euch.hcl.services

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated}
import com.typesafe.config.ConfigFactory
import org.euch.hcl.JobStateFSM.changeJobState
import org.euch.hcl._

import scala.collection.mutable
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Random


//#org.euch.hcl.balancer
class Balancer extends Actor with ActorLogging {

  import scala.concurrent.ExecutionContext.Implicits.global

  val backends = mutable.Set.empty[ActorRef]
  private val buffer = mutable.Set.empty[ActorRef]
  private var jobsToTransfer = List.empty[(ActorRef, Job)]

  override def postStop(): Unit = log.info("Balancer is down")

  override def preStart(): Unit = log.info("Balancer is up")

  def receive: PartialFunction[Any, Unit] = {
    case BackendIsUp(backendRef) if backends add backendRef =>
      context watch backendRef
      assignJobsLater(backendRef)
    case Terminated(actorRef) if backends remove actorRef =>
      assignJobsAndStart()
    case AssignJobsToBackend =>
      flushBuffer()
    case JobsStatusFromBackend(failedJobs) =>
      sender ! JobsDiffForBackend(targetsToWithdraw = failedJobs.map(_.target), jobsToAdd = dequeueJobAssign(sender))


    case wtf => log.warning(s"Received unsupported msg: $wtf")
  }

  private def assignJobsLater(backendRef: ActorRef) = {
    if (buffer.add(backendRef)) {
      log.info(s"Backend registration is buffered: $backendRef")
      context.system.scheduler.scheduleOnce(10 seconds, self, AssignJobsToBackend) //todo
    }
  }

  private def flushBuffer() {
    if (buffer.nonEmpty) {
      assignJobsAndStart()
      buffer.clear()
      log.info("Backend registration buffer flush")
    }
  }

  private def assignJobsAndStart() {
    val tuples: List[(Job, ActorRef)] = Model.fetchAvailableJobs zip (Stream continually backends).flatten
    val actorRefToJobs = tuples.groupBy(_._2).map { case (k, v) => (k, v.map(_._1)) }
    actorRefToJobs.foreach(p => p._1 ! JobsForBackend(p._2.toSet))
  }

  private def dequeueJobAssign(backendRef: ActorRef): Set[Job] = {
    val tuplesToDeque = jobsToTransfer.filter(_._1 == backendRef)
    jobsToTransfer = jobsToTransfer filterNot (tuplesToDeque.contains(_))
    tuplesToDeque.map(_._2).toSet
  }

  private def reassignFailedJob(job: Job): Unit = {
    val failedBackends = job.state.asInstanceOf[BackendFailure].failedBackendRefs
    backends.diff(failedBackends).headOption match {
      case Some(newBackend) =>
        log.info(s"Transfering job to another backend ($job)")
        enqueueJobAssign(newBackend, changeJobState(job, RetryImmediately(failedBackends)))
      case None =>
        log.info(s"Job failed on each backend, will retry after 5 minute or on next job assign ($job)")
        enqueueJobAssign(randomBackend(), changeJobState(job, RetryOnUnlock(5)))
    }
  }

  private def randomBackend() = {
    val rand = new Random(System.currentTimeMillis())
    val random_index = rand.nextInt(backends.size)
    backends.toList(random_index)
  }

  private def enqueueJobAssign(backendRef: ActorRef, job: Job) {
    jobsToTransfer = (backendRef, job) :: jobsToTransfer.filterNot(_._2.target == job.target)
  }

}

//#balancer
object Balancer {

  def main(args: Array[String]): Unit = {
    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [balancer]")).
      withFallback(ConfigFactory.load())

    import akka.actor.OneForOneStrategy
    import akka.actor.SupervisorStrategy.Restart

    import scala.concurrent.duration._
    OneForOneStrategy(10, 2.minutes) {
      case _ => Restart
    }

    val system = ActorSystem("ClusterSystem", config)
    system.actorOf(Props[Balancer], name = "balancer")
  }

}