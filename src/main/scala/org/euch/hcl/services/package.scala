package org.euch.hcl

import akka.actor.{ActorContext, ActorSelection}
import akka.cluster.{Member, MemberStatus}


package object services {
  val balancer = "balancer"

  def registerBackend(context: ActorContext, members: Iterable[Member], mapRequest: BackendIsUp): Option[ActorSelection] = {
    members.filter(_.status == MemberStatus.Up).flatMap(registerBackend(context, _, mapRequest)).headOption
  }

  def registerBackend(context: ActorContext, member: Member, mapRequest: BackendIsUp): Option[ActorSelection] = {
    if (isMemberBalancer(member)) {
      val ref = context.actorSelection(s"${member.address}/user/$balancer")
      ref ! mapRequest
      Some(ref)
    } else
      None
  }

  def isMemberBalancer(member: Member): Boolean = member.hasRole(balancer)
}
