package org.euch.hcl

import akka.actor.ActorRef


final case class Job(target: JobTarget, state: JobState = New)

trait JobState

trait ReadyToExec extends JobState

trait TemporaryBlocked extends JobState

trait Complicated extends JobState

trait ReadyToSave extends JobState {
  val results: Set[TargetResult]
}

case object New extends ReadyToExec

case class RetryOnUnlock(roundsToSkip: Int) extends TemporaryBlocked with Complicated

case object RetryUnlocked extends ReadyToExec with Complicated

final case class RetryImmediately(triedBackends: Set[ActorRef]) extends ReadyToExec

final case class Result(results: Set[TargetResult]) extends ReadyToSave

final case class BackendFailure(throwable: Throwable, failedBackendRefs: Set[ActorRef]) extends Complicated

final case class BackendIsUp(backendRef: ActorRef)

case object AssignJobsToBackend

final case class JobsForBackend(jobs: Set[Job])

final case class JobsStatusFromBackend(failedJobs: Set[Job])

final case class JobsDiffForBackend(targetsToWithdraw: Set[JobTarget], jobsToAdd: Set[Job])


object JobStateFSM {

  def changeJobState(job: Job, newState: JobState): Job = {
    val oldState = job.state

    def changeState: Job = job.copy(state = newState)

    try {

      def successOrFailure = newState match {
        case _: Result => changeState
        case _: BackendFailure => changeState
      }

      def failRecovery = newState match {
        case _: RetryOnUnlock => changeState
        case _: RetryImmediately => changeState
      }

      def unlockAndRetry = newState match {
        case RetryUnlocked => changeState
      }

      oldState match {
        case New => successOrFailure
        case _: RetryImmediately => successOrFailure
        case _: RetryOnUnlock => unlockAndRetry
        case RetryUnlocked => successOrFailure
        case _: BackendFailure => failRecovery
      }
    } catch {
      case _: Throwable =>
        throw new IllegalArgumentException(s"invalid job state transition ${oldState.getClass.getSimpleName} -> ${newState.getClass.getSimpleName}")
    }

  }
}