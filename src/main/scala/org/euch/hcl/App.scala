package org.euch.hcl

import com.typesafe.config.ConfigException.Missing
import com.typesafe.config.ConfigFactory
import org.euch.hcl.services.{Backend, Balancer, Seed}

import scala.collection.JavaConverters._

object App {

  val config = ConfigFactory.load()

  def main(args: Array[String]): Unit = {

    hashshin("seed").foreach(port => Seed.main(Seq(port).toArray))
    hashshin("backend").foreach(port => Backend.main(Seq(port).toArray))
    Thread.sleep(5000)
    hashshin("balancer").foreach(port => Balancer.main(Seq(port).toArray))

  }

  def hashshin(role: String) = try {
    config.getList(s"hashshin.$role").unwrapped().asScala.toList.asInstanceOf[List[String]]
  } catch {
    case e: Missing => List.empty[String]
  }


}