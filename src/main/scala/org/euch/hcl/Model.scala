package org.euch.hcl

import java.time.{Instant, LocalDate}
import javax.sql.DataSource

import com.typesafe.config.{Config, ConfigFactory}
import com.zaxxer.hikari.HikariDataSource
import org.euch.hcl.Model.News.{NewsRecord, NewsTarget}
import org.euch.hcl.Model.Ottd.{OttdServer, OttdStatus}
import org.postgresql.util.PSQLException
import scalikejdbc._

trait JobTarget

trait TargetResult

object Model {
  val cnf: Config = ConfigFactory.load()
  val dataSource: DataSource = {
    val ds = new HikariDataSource()
    ds.setDataSourceClassName(cnf.getString("db.ds"))
    ds.addDataSourceProperty("url", cnf.getString("db.jdbc"))
    ds.addDataSourceProperty("user", cnf.getString("db.user"))
    ds.addDataSourceProperty("password", cnf.getString("db.pass"))
    ds
  }
  ConnectionPool.singleton(new DataSourceConnectionPool(dataSource))

  def fetchAvailableJobs: List[Job] = (News.fetchAllEnabledSources ++ Ottd.fetchAllEnabledSources).map(Job(_))

  def saveJobResult(job: Job): Job = {
    job.state match {
      case s: ReadyToSave =>
        job.target match {
          case newsSource: NewsTarget =>
            News.saveNews(s.results.asInstanceOf[Set[NewsRecord]].toSeq, newsSource)
          case ottdServer: OttdServer =>
            Ottd.saveStatus(s.results.head.asInstanceOf[OttdStatus], ottdServer)
        }
      case _ =>
    }
    job
  }

  implicit val session = AutoSession

  private def saveDate = Instant.now

  object News {

    @throws(classOf[PSQLException])
    def fetchAllEnabledSources: List[NewsTarget] = sql"select * from news_source where enabled is true".map(rs => NewsTarget(rs)).list.apply()

    @throws(classOf[PSQLException])
    def saveNews(newsRecords: Seq[NewsRecord], newsSource: NewsTarget) {
      newsRecords.
        reverse.headOption match {
        case None =>
        case Some(_) => newsRecords.foreach(doSave(newsSource, _))
      }

      def doSave(ns: NewsTarget, r: NewsRecord) {
        val content = if (ns.mirrorContent) r.content
        sql"INSERT INTO public.news(id, link, save_date, title, news_source_parent_fk, content, pub_date) VALUES (nextval('default_sequence'), ${r.link}, $saveDate, ${r.title}, ${ns.id}, $content, ${r.pubDate}) ON CONFLICT DO NOTHING".update.apply()
      }
    }

    case class NewsRecord(link: String, title: String, content: String, pubDate: Instant) extends TargetResult

    case class NewsTarget(id: Long,
                          url: String,
                          enabled: Boolean,
                          mirrorContent: Boolean,
                          name: String) extends JobTarget

    object NewsTarget extends SQLSyntaxSupport[NewsTarget] {
      override val tableName = "news_source"

      def apply(rs: WrappedResultSet): NewsTarget = new NewsTarget(
        rs.long("id"), rs.string("url"), rs.boolean("mirror_content"), rs.boolean("enabled"), rs.string("name"))
    }

  }

  object Ottd {

    @throws(classOf[PSQLException])
    def fetchAllEnabledSources: List[OttdServer] = sql"select * from ottd_source where enabled is true".map(rs => OttdServer(rs)).list.apply()

    @throws(classOf[PSQLException])
    def saveStatus(st: OttdStatus, src: OttdServer) {
      sql"INSERT INTO public.ottd_status(id, probe_date, online, companies_online, players_online, game_date, ottd_source) VALUES (nextval('default_sequence'), $saveDate, ${st.online}, ${st.companies}, ${st.players}, ${st.date}, ${src.id}) ON CONFLICT DO NOTHING".update.apply()
    }

    case class OttdStatus(online: Boolean, companies: Option[Int], players: Option[Int], date: Option[LocalDate]) extends TargetResult

    case class OttdServer(id: Long, name: String, enabled: Boolean) extends JobTarget

    object OttdServer extends SQLSyntaxSupport[OttdServer] {
      override val tableName = "ottd_source"

      def apply(rs: WrappedResultSet): OttdServer = new OttdServer(rs.long("id"), rs.string("name"), rs.boolean("enabled"))
    }

  }

}
