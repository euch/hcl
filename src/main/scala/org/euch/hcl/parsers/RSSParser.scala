package org.euch.hcl.parsers

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import org.euch.hcl.Model.News.{NewsRecord, NewsTarget}

object RSSParser {

  import scala.xml.{SAXParseException, XML}

  def parse(ns: NewsTarget): Set[NewsRecord] = try {
    (XML.load(ns.url) \\ "item").map(
      node => {
        NewsRecord(
          link = (node \\ "link").text,
          title = (node \\ "title").text,
          content = (node \\ "description").text,
          pubDate = ZonedDateTime.parse(
            (node \\ "pubDate").text,
            DateTimeFormatter.RFC_1123_DATE_TIME
          ).toInstant
        )
      }).toSet
  } catch {
    case s: SAXParseException => throw new Exception("Invalid feed format", s)
  }

}
