package org.euch.hcl.parsers

import java.io.IOException
import java.net.URL
import java.time.LocalDate

import org.apache.commons.lang3.StringEscapeUtils
import org.euch.hcl.Model.Ottd.{OttdServer, OttdStatus}
import org.htmlcleaner.{ContentNode, HtmlCleaner, TagNode}

object OpenTTDStatusParser {
  private lazy val cleaner = new HtmlCleaner

  def parse(src: OttdServer): Set[OttdStatus] = try {
    cleaner.clean(new URL("https://www.openttd.org/en/servers")).getElementsByName("tr", true)
      .filter(elem => StringEscapeUtils.unescapeHtml4(elem.getText.toString).contains(src.name))
      .map(elem => "https://www.openttd.org/" + elem.getAllChildren.get(3).asInstanceOf[TagNode].getAllChildren.get(0).asInstanceOf[TagNode].getAllChildren.get(0).asInstanceOf[TagNode].getAttributeByName("href"))
      .headOption match {
      case None =>
        Set(OttdStatus(online = false, None, None, None))
      case Some(serverDescriptionUrl) =>
        lazy val fields = cleaner.clean(new URL(serverDescriptionUrl)).getElementsByName("tr", true).toList

        def fieldText(name: String): Option[String] = fields.filter(_.getAllChildren.get(0) match {
          case tn: TagNode =>
            tn.getAllChildren.get(0) match {
              case cn: ContentNode =>
                cn.getContent.equals(name)
              case _ =>
                false
            }
          case _ =>
            false
        }).map(_.getAllChildren.get(1).asInstanceOf[TagNode].getText.toString).headOption

        lazy val date = fieldText("Current date:") match {
          case None =>
            None
          case Some(d) =>
            Some(LocalDate.parse(d))
        }
        lazy val (clients, companies) = fieldText("Clients:") match {
          case None =>
            (None, None)
          case Some(c) =>
            val a = c.split("[^\\d]").toSeq.flatMap(s => try Some(Integer.parseInt(s.trim))
            catch {
              case _: Exception => None
            })
            (Some(a.head), Some(a(2)))
        }
        Set(OttdStatus(online = true, companies, clients, date))

    }
  } catch {
    case th: IOException =>
      throw new Throwable("Failed to open openttd server page", th)
  }

}
