package org.euch.hcl

import org.euch.hcl.JobStateFSM.changeJobState
import org.euch.hcl.Model.News.NewsTarget
import org.euch.hcl.Model.Ottd.OttdServer

package object parsers {

  @throws(classOf[Throwable])
  def exec(job: Job): Job =
    job.state match {
      case _: ReadyToExec =>
        changeJobState(job, Result(results =
          (job.target match {
            case s: NewsTarget => RSSParser.parse(s)
            case s: OttdServer => OpenTTDStatusParser.parse(s)
          }).asInstanceOf[Set[TargetResult]]
        ))
      case _ => job
    }


}
