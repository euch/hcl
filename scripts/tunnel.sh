#!/bin/bash


ssh -N -o ExitOnForwardFailure=yes \
-L 2551:localhost:2551 \
-L 2552:localhost:2552 \
-L 2553:localhost:2553 \
-L 2554:localhost:2554 \
-L 5432:localhost:5432 \
-R 4001:localhost:4001 \
-R 4002:localhost:4002 \
root@kabak.ddns.net -p 9122
