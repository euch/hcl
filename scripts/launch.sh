#!/bin/bash

trap 'kill -TERM $PID' TERM INT
CFG=`pwd`/conf/$1.conf
echo "using config: $CFG"
cat $CFG
echo "hashshin!"
java -Xms128m -Xmx512m -Djava.library.path=./native -Dconfig.file=$CFG -cp hcl-assembly-*.jar org.euch.hcl.App &
PID=$!
wait $PID
trap - TERM INT
wait $PID
EXIT_STATUS=$?