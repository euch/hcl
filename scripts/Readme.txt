`*.service` files are for systemd
To use them, you should create working directory (`/opt/hcl` by default)
In home directory, you should put:
 1. directory named `conf` with empty files `backend.conf`, `seed.conf` and `balancer.conf`
 2. `hcl-assembly-[version].jar` file
 3. `launch.sh` file from this directory


`tunnel.sh` is an example for starting tunnel between different servers.
    In upper part you should specify all the node ports on remote machine
    In lower part you should specify nodes on the current host

Ports are configurable in .application.conf and in each overriding config (`/opt/hcl/conf*`
